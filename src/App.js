import './App.css';
import {once} from 'lodash';

function App() {
  let totalsum = 0;

  /**
   * Add method which takes two numeric values as input
  */
  const add = (a, b) => {
    return a + b;
  }

  /**
   * My Once method which takes function as input
  */
  const myOnce = func => {
    return once(func)
  }

  const onceAdd = myOnce(add);
  totalsum = onceAdd(14, 5);

  return (
    <div className="App">
      <b>Total Sum :</b> <span>{totalsum}</span>
    </div>
  );
}

export default App;
